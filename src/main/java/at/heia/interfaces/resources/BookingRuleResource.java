package at.heia.interfaces.resources;

import at.heia.interfaces.api.bookingrule.request.OTAHotelBookingRuleNotifRQType;
import at.heia.interfaces.api.bookingrule.response.OTAHotelBookingRuleNotifRSType;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 * Created by Rasha on 22.05.2018.
 */
@Path("/booking-rules")
@Consumes({MediaType.TEXT_XML, MediaType.APPLICATION_XML})
@Produces(MediaType.APPLICATION_XML)
public class BookingRuleResource {

    @POST
    public OTAHotelBookingRuleNotifRSType uploadBookingRules(OTAHotelBookingRuleNotifRQType otaHotelBookingRuleNotifRQType) {
        OTAHotelBookingRuleNotifRSType response = new OTAHotelBookingRuleNotifRSType();
        response.setSuccess("true");
        response.setVersion("1.002");
        response.setTimeStamp(new Date().toString());
        return response;
    }
}
