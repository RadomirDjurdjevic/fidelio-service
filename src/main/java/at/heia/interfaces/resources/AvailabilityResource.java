package at.heia.interfaces.resources;

import at.heia.interfaces.api.availability.request.OTAHotelAvailNotifRQType;
import at.heia.interfaces.api.availability.response.OTAHotelAvailNotifRSType;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 * Created by Rasha on 21.05.2018.
 */
@Path("/availability")
@Consumes({MediaType.TEXT_XML, MediaType.APPLICATION_XML})
@Produces(MediaType.APPLICATION_XML)
public class AvailabilityResource {

    @POST
    public OTAHotelAvailNotifRSType uploadAvailability(OTAHotelAvailNotifRQType otaHotelAvailNotifRQType) {
        OTAHotelAvailNotifRSType response = new OTAHotelAvailNotifRSType();
        response.setSuccess("true");
        response.setTimeStamp(new Date().toString());
        response.setVersion("1.002");
        return response;
    }
}
