package at.heia.interfaces;

import at.heia.interfaces.resources.AvailabilityResource;
import at.heia.interfaces.resources.BookingRuleResource;
import at.heia.interfaces.resources.RatePlanResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.filter.LoggingFilter;

import java.util.logging.Logger;

public class FidelioServiceApplication extends Application<FidelioServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new FidelioServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "FidelioService";
    }

    @Override
    public void initialize(final Bootstrap<FidelioServiceConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final FidelioServiceConfiguration configuration,
                    final Environment environment) {
        // instantiate resources
        final AvailabilityResource availResource = new AvailabilityResource();
        final BookingRuleResource bookingRuleResource = new BookingRuleResource();
        final RatePlanResource ratePlanResource = new RatePlanResource();

        // register resources
        environment.jersey().register(availResource);
        environment.jersey().register(bookingRuleResource);
        environment.jersey().register(ratePlanResource);

        // logging inbound request/response
        environment.jersey().register(new LoggingFilter(Logger.getLogger("InboundRequestResponse"), true));
    }
}
