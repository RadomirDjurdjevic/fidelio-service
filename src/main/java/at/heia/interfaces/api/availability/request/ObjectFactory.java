//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.19 at 07:58:24 PM CEST 
//


package at.heia.interfaces.api.availability.request;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fidelio.avail.classes package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OTAHotelAvailNotifRQ_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelAvailNotifRQ");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fidelio.avail.classes
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OTAHotelAvailNotifRQType }
     * 
     */
    public OTAHotelAvailNotifRQType createOTAHotelAvailNotifRQType() {
        return new OTAHotelAvailNotifRQType();
    }

    /**
     * Create an instance of {@link AvailStatusMessageType }
     * 
     */
    public AvailStatusMessageType createAvailStatusMessageType() {
        return new AvailStatusMessageType();
    }

    /**
     * Create an instance of {@link StatusApplicationControlType }
     * 
     */
    public StatusApplicationControlType createStatusApplicationControlType() {
        return new StatusApplicationControlType();
    }

    /**
     * Create an instance of {@link POSType }
     * 
     */
    public POSType createPOSType() {
        return new POSType();
    }

    /**
     * Create an instance of {@link AvailStatusMessagesType }
     * 
     */
    public AvailStatusMessagesType createAvailStatusMessagesType() {
        return new AvailStatusMessagesType();
    }

    /**
     * Create an instance of {@link SourceType }
     * 
     */
    public SourceType createSourceType() {
        return new SourceType();
    }

    /**
     * Create an instance of {@link RequestorIDType }
     * 
     */
    public RequestorIDType createRequestorIDType() {
        return new RequestorIDType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelAvailNotifRQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelAvailNotifRQ")
    public JAXBElement<OTAHotelAvailNotifRQType> createOTAHotelAvailNotifRQ(OTAHotelAvailNotifRQType value) {
        return new JAXBElement<OTAHotelAvailNotifRQType>(_OTAHotelAvailNotifRQ_QNAME, OTAHotelAvailNotifRQType.class, null, value);
    }

}
