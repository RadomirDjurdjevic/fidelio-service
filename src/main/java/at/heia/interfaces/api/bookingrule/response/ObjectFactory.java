//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.22 at 11:17:02 PM CEST 
//


package at.heia.interfaces.api.bookingrule.response;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the response package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OTAHotelBookingRuleNotifRS_QNAME = new QName("http://www.opentravel.org/OTA/2003/05", "OTA_HotelBookingRuleNotifRS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: response
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OTAHotelBookingRuleNotifRSType }
     * 
     */
    public OTAHotelBookingRuleNotifRSType createOTAHotelBookingRuleNotifRSType() {
        return new OTAHotelBookingRuleNotifRSType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OTAHotelBookingRuleNotifRSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.opentravel.org/OTA/2003/05", name = "OTA_HotelBookingRuleNotifRS")
    public JAXBElement<OTAHotelBookingRuleNotifRSType> createOTAHotelBookingRuleNotifRS(OTAHotelBookingRuleNotifRSType value) {
        return new JAXBElement<OTAHotelBookingRuleNotifRSType>(_OTAHotelBookingRuleNotifRS_QNAME, OTAHotelBookingRuleNotifRSType.class, null, value);
    }

}
